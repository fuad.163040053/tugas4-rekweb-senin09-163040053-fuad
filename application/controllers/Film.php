<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Restoran extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		// ketikan source yang ada di modul
		$this->API = "http://localhost/Rest_Server/film-api/film";
	}

// untuk halaman ADMIN
	public function index()
	{
		// ketikan source yang ada di modul
		$data['judul'] = 'Halaman Film';
		$data['film'] = json_decode($this->curl->simple_get($this->API . '/tabel_film'));
		$data['fil'] = $data['tabelfilm'];
		$data['content'] = 'tabel_film/tabel_film';
		$this->load->view('template/template',$data);
	}

	public function search()
	{
		// ketikan source yang ada di modul
		$search = urldecode($this->input->get("keyword", true));
		$data['judul'] = 'Halaman Film';
		$data['film'] = json_decode($this->curl->simple_get($this->API . '/tabel_film/search/?cari=' .urldecode($search)));
		$data['fil'] = $data['tabelfilm'];
		$data['content'] = 'tabel_film/tabel_film';
		$this->load->view('template/template',$data);
	}

	public function create()
	{
		// ketikan source yang ada di modul
		 $judul = $this->post('judul');
        $gambar = $this->post('gambar');
        $genre = $this->post('genre');
        $negara = $this->post('negara');
        $tahun = $this->post('tahun');
        $stok = $this->post('stok');
        $harga = $this->post('harga');
        $sinopsis = $this->post('sinopsis');
        
        $data = array(
            'judul' => $judul,
            'gambar' => $gambar,
            'genre' => $genre,
            'negara' => $negara,
            'tahun' => $tahun,
            'stok' => $stok,
            'harga' => $harga,
            'sinopsis' => $sinopsis
        );

		$this->curl->simple_post($this->API . '/tabel_film/', $data,array(CURLOPT_BUFFERSIZE => 10));
		echo json_encode(array("status" =>TRUE));
	}

	public function edit()
	{
		// ketikan source yang ada di modul
		$id = $this->input->post("id");
		$params = array('id' =>$id);
		$data['tabelfilm'] = json_decode($this->curl->simple_get($this->API . '/tabel_film/' , $params));
		$tabelfilm = $data['tabelfilm'];
		$json = json_encode(array("status" => 200, "fil" => $tabelfilm));
		echo $json;
	}

	public function update()
	{
		// ketikan source yang ada di modul
		 $judul = $this->post('judul');
        $gambar = $this->post('gambar');
        $genre = $this->post('genre');
        $negara = $this->post('negara');
        $tahun = $this->post('tahun');
        $stok = $this->post('stok');
        $harga = $this->post('harga');
        $sinopsis = $this->post('sinopsis');
		$id = $this->input->post("id");

		$data = array(
			'id' =>$id,
            'judul' => $judul,
            'gambar' => $gambar,
            'genre' => $genre,
            'negara' => $negara,
            'tahun' => $tahun,
            'stok' => $stok,
            'harga' => $harga,
            'sinopsis' => $sinopsis
        );
		$this->curl->simple_put($this->API . '/tabel_film/', $data,array(CURLOPT_BUFFERSIZE => 10));
		echo json_encode(array("status" =>TRUE));
	}

	public function delete()
	{
		// ketikan source yang ada di modul
		$id = $this->input->post('id');
		json_decode($this->curl->simple_delete($this->API . '/tabel_film/', array('id' => $id), array(CURLOPT_BUFFERSIZE => 10)));
		echo json_encode(array("status" =>TRUE));
	}
}
