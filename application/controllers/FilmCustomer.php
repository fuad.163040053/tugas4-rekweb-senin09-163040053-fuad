<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class RestoranCustomer extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		// ketikan source yang ada di modul
		$this->API = "http://localhost/rest_server_restoran/restoran";
	}

// untuk halaman ADMIN
	public function index()
	{
		// ketikan source yang ada di modul
		$data['judul'] = 'Halaman Makanan dan Minuman';
		$data['makandanminum'] = json_decode($this->curl->simple_get($this->API . '/restoranCustomer'));
		$data['mdm'] = $data['makandanminum'];
		$data['content'] = 'restoranCustomer/restoranCustomer';
		$this->load->view('template/templateCustomer',$data);
	}

	public function search()
	{
		// ketikan source yang ada di modul
		$search = urldecode($this->input->get("keyword", true));
		$data['judul'] = 'Halaman Makanan dan Minuman';
		$data['makandanminum'] = json_decode($this->curl->simple_get($this->API . '/restoranCustomer/search/?cari=' .urldecode($search)));
		$data['mdm'] = $data['makandanminum'];
		$data['content'] = 'restoranCustomer/restoranCustomer';
		$this->load->view('template/templateCustomer',$data);
	}

	// untuk Transaksi
	function add(){
		$this->load->library("cart");
		$data = array(
		   "id"  => $_POST["id"],
		   "NamaMakanDanMinum"  => $_POST["NamaMakanDanMinum"],
		   "JenisMakanDanMinum"  => $_POST["JenisMakanDanMinum"],
		   "JumlahMakanDanMinum"  => $_POST["JumlahMakanDanMinum"],
		   "HargaMakanDanMinum"  => $_POST["HargaMakanDanMinum"]
		);
		$this->cart->insert($data);
		echo $this->view();
	}

	function load(){
		echo $this->view();
	}

	function view(){
		$this->load->library("cart");
		$output = '';
		$output .= '
		  <h3>Shopping Cart</h3><br />
		  <div class="table-responsive">
		   <div align="right">
		    <button type="button" id="clear_cart" class="btn btn-warning">Clear Cart</button>
		   </div>
		   <br />
		   <table class="table table-bordered">
		    <tr>
		     <th width="40%">Nama</th>
		     <th width="10%">Jenis</th>
		     <th width="10%">Jumlah</th>
		     <th width="10%">Harga</th>
		     <th width="20%">Total</th>
		     <th width="10%">Action</th>
		    </tr>

		  ';
		  $count = 0;
		  foreach($this->cart->contents() as $key)
		  {
		   $count++;
		   $output .= '
		   <tr> 
		    <td>'.$key["NamaMakanDanMinum"].'</td>
		    <td>'.$key["JenisMakanDanMinum"].'</td>
		    <td>'.$key["JumlahMakanDanMinum"].'</td>
		    <td>'.$key["HargaMakanDanMinum"].'</td>
		    <td>'.$key["subtotal"].'</td>
		    <td><button type="button" name="remove" class="btn btn-danger btn-xs remove_inventory" id="'.$key["id"].'">Remove</button></td>
		   </tr>
		   ';
		  }
		  $output .= '
		   <tr>
		    <td colspan="4" align="right">Total</td>
		    <td>'.$this->cart->total().'</td>
		   </tr>
		  </table>

		  </div>
		  ';

		  if($count == 0)
		  {
		   $output = '<h3 align="center">Cart is Empty</h3>';
		  }
		  return $output;
	}
}
