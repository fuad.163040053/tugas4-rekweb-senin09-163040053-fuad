<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="UTF-8">
		<title><?= $judul ?></title>
		<link rel="stylesheet" href="<?= base_url('assets/css/bootstrap.css') ?>">
		<link rel="stylesheet" href="<?= base_url('assets/css/bootstrap.min.css') ?>">
		<link rel="stylesheet" href="<?= base_url('assets/css/style.css') ?>">
	</head>
	<body>
		<div id="header">
			<div class="navbar navbar-expand-lg navbar-light bg-light navbar-top">
				<div class="container">
							<ul class="navbar-nav">
								<a class="navbar-brand" href="<?= site_url() ?>">NICE DREAM</a>
								<form action="<?= site_url('/restoranCustomer/search/') ?>" method="get">
									<div class="form-group">
										<div class="input-group">
											<input type="text" name="keyword" class="form-control" placeholder="Search...">
											<div class="input-group-btn">
												<button type="submit" class="btn btn-primary"
														style="border-top-left-radius: 0px; border-bottom-left-radius: 0px;">Search
												</button>
											</div>
										</div>
									</div>
								</form>
							</ul><!--penutup ul class navbar-nav -->
							<ul>
			                    <a href="<?= site_url('restoranCustomer/cart') ?>"> <i class="menu-icon ti-email"></i>Transaktion </a>
								<!-- <a href="<?= site_url("todo/loginCustomer")?>" id="login_pop"> Login</a> -->
							</ul>
				</div> <!--penutup div class container-->
			</div>
		</div>
