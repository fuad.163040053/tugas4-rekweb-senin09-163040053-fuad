<!-- Modal Tambah -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
	 aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalLabel">Tambah Film</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<form action="" id="formTambah" method="post">
					<div class="form-group">
						<label for="judul">Judul</label>
						<input type="text" class="form-control" name="judul" id="judul"
							   placeholder="Masukan Judul">
					</div>
					<div class="form-group">
						<label for="gambar">Gambar</label>
						<input type="text" class="form-control" id="gambar" aria-describedby="emailHelp"
							   name="gambar" placeholder="Masukan Gambar">
					</div>
					<div class="form-group">
						<label for="genre">Genre</label>
						<input type="text" class="form-control" name="genre" id="genre"
							   placeholder="Masukan Genre">
					</div>
					<div class="form-group">
						<label for="negara">Negara</label>
						<input type="text" class="form-control" name="negara" id="negara"
							   placeholder="Masukan Negara">
					</div>
					<div class="form-group">
						<label for="tahun">Tahun</label>
						<input type="text" class="form-control" name="tahun" id="tahun"
							   placeholder="Masukan Tahun">
					</div>
					<div class="form-group">
						<label for="stok">Stok</label>
						<input type="text" class="form-control" name="stok" id="stok"
							   placeholder="Masukan Stok">
					</div>
					<div class="form-group">
						<label for="harga">Harga</label>
						<input type="text" class="form-control" name="harga" id="harga"
							   placeholder="Masukan Harga">
					</div>
					<div class="form-group">
						<label for="sinopsis">Sinopsis</label>
						<input type="text" class="form-control" name="sinopsis" id="sinopsis"
							   placeholder="Masukan Sinopsis">
					</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
				<button type="submit" id="submitAdd" class="btn btn-primary" name="add">Simpan</button>
			</div>
			</form>
		</div>
	</div>
</div>


<!-- Modal Update -->
<div class="modal fade" id="modalUpdate" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel1"
	 aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalLabel1">Update Film</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<form action="#" id="formUpdate" method="post">
					<input type="hidden" name="" id="id_fil">
					<div class="form-group">
						<label for="judulUpdate">Judul</label>
						<input type="text" value="" class="form-control" id="judulUpdate"
							    name="judulUpdate" placeholder="Masukan judul">
					</div>
					<div class="form-group">
						<label for="gambarUpdate">Gambar</label>
						<input type="text" value="" class="form-control" name="gambarUpdate" id="gambarUpdate"
							   placeholder="Masukan Gambar">
					</div>
					<div class="form-group">
						<label for="genreUpdate">Genre</label>
						<input type="text" value="" class="form-control" id="genreUpdate"
							    name="genreUpdate" placeholder="Masukan Genre">
					</div>
					<div class="form-group">
						<label for="negaraUpdate">Negara</label>
						<input type="text" value="" class="form-control" name="negaraUpdate" id="negaraUpdate"
							   placeholder="Masukan Negara">
					</div>
					<div class="form-group">
						<label for="tahunUpdate">Tahun</label>
						<input type="text" value="" class="form-control" name="tahunUpdate" id="tahunUpdate"
							   placeholder="Masukan Tahun">
					</div>
					<div class="form-group">
						<label for="stokUpdate">Stok</label>
						<input type="text" value="" class="form-control" name="stokUpdate" id="stokUpdate"
							   placeholder="Masukan Stok">
					</div>
					<div class="form-group">
						<label for="sinopsisUpdate">Sinopsis</label>
						<input type="text" value="" class="form-control" name="sinopsisUpdate" id="sinopsisUpdate"
							   placeholder="Masukan Sinopsis">
					</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
				<button type="submit" class="btn btn-primary" name="add" id="submitUpdate">Simpan</button>
			</div>
			</form>
		</div>
	</div>
</div>


<div class="container mt-5">
	<div class="row">
		<div class="col-12">
			<h3 style="text-align: center;">Daftar Film</h3>
			<button class="btn btn-success btn-sm" id="buttonAdd" data-toggle="modal" data-target="#exampleModal">
				Tambah
			</button>
			<div class="col-3" style="float: right; position: relative; left: 15px">
				<form action="<?= site_url('/restoran/search/') ?>" method="get">
					<div class="form-group">
						<div class="input-group">
							<input type="text" name="keyword" class="form-control" placeholder="Search...">
							<div class="input-group-btn">
								<button type="submit" class="btn btn-primary"
										style="border-top-left-radius: 0px; border-bottom-left-radius: 0px;">Search
								</button>
							</div>
						</div>
					</div>
				</form>
			</div>
			<!-- ketikan source yang ada di modul -->
			<table class="table table-striped table-bordered" id="mytable" style="margin-top: 20px">
				<thead class="thead-dark">
					<th scope="col" style="text-align: center;">No</th>
					<th scope="col" style="text-align: center;">Judul</th>
					<th scope="col" style="text-align: center;">Gambar</th>
					<th scope="col" style="text-align: center;">Genre</th>
					<th scope="col" style="text-align: center;">Negara</th>
					<th scope="col" style="text-align: center;">Tahun</th>
					<th scope="col" style="text-align: center;">Stok</th>
					<th scope="col" style="text-align: center;">Harga</th>
					<th scope="col" style="text-align: center;">Sinopsis</th>
				</thead>
				<?php if (empty($fil)): ?>
					<tr>
						<td colspan="6" style="text-align: center;">Maaf data yang anda cari tidak di temukan</td>
					</tr>
					<?php else : ?>
						<?php $no = 1 ?>
						<?php foreach ($fil as $key) : ?>
							<tr>
								<td><?= $no++ ?></td>
								<td><?= $key->judul ?></td>
								<td><img class="card-img-top" src="<?= site_url("assets/img/")?><?= $key->gambar?>" alt="Card image cap"></td>
								<td><?= $key->genre ?></td>
								<td><?= $key->negara ?></td>
								<td><?= $key->tahun ?></td>
								<td><?= $key->stok ?></td>
								<td><?= $key->harga ?></td>
								<td><?= $key->sinopsis ?></td>
								<td style="text-align: center;">
									<a href="" class="btn btn-sm btn-success update" id="<?= $key->id ?>">Update</a>
									<a href="" class="btn btn-sm btn-danger delete" id="<?= $key->id ?>">Delete</a>
								</td>
							</tr>
						<?php endforeach ?>
				<?php endif; ?>
			</table>
		</div>
	</div>
</div>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="<?= base_url('assets/js/sweetalert.min.js') ?>"></script>
<script>
	$(document).ready(function () {
		
		$("#submitAdd").click(function (e) {
			// ketikan source yang ada di modul
			e.preventDefault();
			$.ajax({
				url: "<?= site_url('tabel_film/create') ?>",
				type: "POST",
				dataType: "JSON",
				data: $("#formTambah").serialize(),
				success: function(){
					$('#exampleModal').modal('hide');
					swal({
						title: "Success",
						text : "Data Berhasil Disimpan",
						icon: "success",
						buttons: false,
					});
					setTimeOut(function(){
						location.reload();
					}, 2000);
				},
				error:function(xhr, status, error){
					alert(status + " : "+error);
				}
			});
		});

		$(".update").click(function (e) {
			// ketikan source yang ada di modul
			e.preventDefault();
			$("#modalUpdate").modal('show');
			var id = $(this).attr("id");
			$.ajax({
				url: "<?= site_url('tabel_film/edit/') ?>",
				type: "POST",
				data: "id=" + id,
				dataType: "JSON",
				success: function(data){
					if (data.status == 200) {
						$("#id_fil").val(data.fil[0].id);
						$("#judulUpdate").val(data.fil[0].judul);
						$("#gambarUpdate").val(data.fil[0].gambar);
						$("#genreUpdate").val(data.fil[0].genre);
						$("#negaraUpdate").val(data.fil[0].negara);
						$("#tahunUpdate").val(data.fil[0].tahun);
						$("#stokUpdate").val(data.fil[0].stok);
						$("#hargaUpdate").val(data.fil[0].harga);
						$("#sinopsisUpdate").val(data.fil[0].sinopsis);
					}
				},
				error:function(xhr, status, error){
					alert(status + " : "+error);
				}
			});
		});
		});

		$("#submitUpdate").click(function (e) {
			// ketikan source yang ada di modul
			e.preventDefault();
			$.ajax({
				url: "<?= site_url('restoran/update') ?>",
				type: "POST",
				data: $("#formUpdate").serialize(),
				dataType: "JSON",
				success: function(){
					$('#exampleModal').modal('hide');
					swal({
						title: "Success",
						text : "Data Berhasil DiUpdate",
						icon: "success",
						buttons: false,
					});
					setTimeOut(function(){
						location.reload();
					}, 2000);
				},
				error:function(xhr, status, error){
					alert(status + " : "+error);
				}
			});
		});

		$(".delete").click(function (e) {
			// ketikan source yang ada di modul
			e.preventDefault();
			var id = $(this).attr("id");
			swal({
				title: "Apakah Anda Yakin Ingin Menghapus ?",
				text : "Data yang Terhapus tidak bisa Dipulihkan",
				icon: "Warning",
				buttons: true,
				dangerMode: true,
				})
					.then((willDelete)=>{
						if (willDelete) {
							$.ajax({
							url: "<?= site_url('restoran/delete') ?>",
							type: "POST",
							data: "id=" + id,
							dataType: "JSON",
							success: function(data){
								if (data.status == true) {
									swal({
									title: "Success",
									text : "Data Berhasil Dihapus",
									icon: "success",
									buttons: false,
								});
								setTimeOut(function(){
									location.reload();
								}, 2000);
						}
					},
				error:function(xhr, status, error){
					alert(status + " : "+error);
				}
			});
		} else {
			swal("Batal Menghapus Data");
		}
		});
		});
</script>


