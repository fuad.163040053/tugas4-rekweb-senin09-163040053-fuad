<div id="main-content">
		<div class="row" style="padding-left: 15px; padding-right: 15px; margin-left: 0; margin-right: 0">
			<table>
			<!-- ketikan source yang ada di modul -->
				<?php if (empty($mdm)): ?>
					<tr>
						<td colspan="6" style="text-align: center;">Maaf data yang anda cari tidak di temukan</td>
					</tr>
					<?php else : ?>
						<?php foreach ($mdm as $key) : ?>
							<div class="col-3">
								<div class="card">
									<img class="card-img-top" src="<?= site_url("assets/img/")?><?= $key->GambarMakanDanMinum ?>" alt="Card image cap">
									<div class="card-body">
										<h5 class="card-title"><?= $key->NamaMakanDanMinum?></h5>
											<p class="card-text">Jenis : <?= $key->JenisMakanDanMinum?></p>
											<p><?= $key->DetailMakanDanMinum ?></p>

											<p class="card-text">Jumlah : <?= $key->JumlahMakanDanMinum ?></p>

											<p class="card-text">Harga : <?= $key->HargaMakanDanMinum ?></p>

											<input type="text" name="JumlahMakanDanMinum" class="JumlahMakanDanMinum" id="'.$key->id.'"/>

											<button type="button" name="add_cart" class="btn btn-success add_cart"NamaMakanDanMinum="<?= $key->NamaMakanDanMinum ?>" JenisMakanDanMinum="<?= $key->JenisMakanDanMinum ?>" JumlahMakanDanMinum="<?= $key->JumlahMakanDanMinum ?>"HargaMakanDanMinum="<?= $key->HargaMakanDanMinum ?>" id="<?= $key->id ?>">Add to Cart
											</button>
									</div>
								</div>
							</div>
						<?php endforeach; ?>
				<?php endif; ?>
		</table>
	</div>
</div>
<div>
	<div id="cart_details">
		<h3 align="center">Cart is Empty</h3>
	</div>	
</div>

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="<?= base_url('assets/js/sweetalert.min.js') ?>"></script>
<script>
	$(document).ready(function () {

		$('.add_cart').click(function () {
			var id = $(this).data("id");
			var NamaMakanDanMinum = $(this).data("NamaMakanDanMinum");
			var JenisMakanDanMinum = $(this).data("JenisMakanDanMinum");
			var JumlahMakanDanMinum = $('#' + id).val();
			var HargaMakanDanMinum = $(this).data("HargaMakanDanMinum");
			if (JumlahMakanDanMinum != '' && JumlahMakanDanMinum > 0) {
				$.ajax({
					url:"<?= site_url('restoranCustomer/add')  ?>",
					method: "POST",
					data: "id=" + id, 
					success:function(data){
						alert("Produk masuk ke dalam Cart");
						$('#cart_details').html(data);
						$('#'+ id).val('');
					}
				});
			}
			else {
				alert("Tolong Masukan Jumlah Makanan atau Minuman");
			}
		});
		$('#cart_details').load("<?= base_url(); ?>restoranCustomer/load");
	});
</script>