-- phpMyAdmin SQL Dump
-- version 4.6.5.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Dec 28, 2018 at 11:09 AM
-- Server version: 10.1.21-MariaDB
-- PHP Version: 7.1.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_film`
--

-- --------------------------------------------------------

--
-- Table structure for table `tabel_customer`
--

CREATE TABLE `tabel_customer` (
  `id_customer` int(11) NOT NULL,
  `nama` varchar(100) NOT NULL,
  `alamat` varchar(100) NOT NULL,
  `email` int(50) NOT NULL,
  `jenis_kelamin` varchar(30) NOT NULL,
  `telp` varchar(12) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tabel_film`
--

CREATE TABLE `tabel_film` (
  `id` int(3) NOT NULL,
  `judul` varchar(50) NOT NULL,
  `gambar` varchar(20) NOT NULL,
  `genre` varchar(20) NOT NULL,
  `negara` varchar(15) NOT NULL,
  `tahun` date NOT NULL,
  `stok` int(5) NOT NULL,
  `harga` int(6) NOT NULL,
  `sinopsis` varchar(750) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tabel_film`
--

INSERT INTO `tabel_film` (`id`, `judul`, `gambar`, `genre`, `negara`, `tahun`, `stok`, `harga`, `sinopsis`) VALUES
(1, 'Ant-Man', 'ant.png', 'Action', 'USA', '2018-07-05', 7, 75000, 'Scott Lang sebagai Pahlawan Super dan ayah. Hope van Dyne dan Dr. Hank Pym menyajikan misi baru yang mendesak yang menemukan Ant-Man bertarung bersama The Wasp untuk mengungkap rahasia dari masa lalu mereka.'),
(2, 'The Boss Baby', 'bossbaby.png', 'Animasi', 'USA', '2017-03-23', 3, 50000, 'The Boss Baby adalah cerita yang sangat lucu tentang bagaimana kelahiran bayi yang baru berdampak pada keluarga, diceritakan dari sudut pandang seorang narator yang menyenangkan, seorang Tim yang berusia 7 tahun yang sangat imajinatif. Dengan pesan licik dan penuh hati tentang pentingnya keluarga.'),
(3, 'Venom', 'venom.png', 'Action', 'USA', '2018-10-05', 10, 70000, 'Eddie ingin melakukan sebuah investasi kasus terhadap sebuah penemuan. Sialnya, symbiote  malah masuk ke dalam tubuhnya. Tubuh Eddie kemudian berubah. Eddie menemukan kekuatan super dalam dirinya dari organisme hitam tersebut. Dirinya dapat mengendalikan semua hal yang ia lakukan.'),
(4, 'The Nun', 'thenun.png', 'Horor', 'USA', '2018-09-07', 7, 60000, 'Ketika seorang biarawati muda di biara terpencil di Rumania mengambil hidupnya sendiri, seorang imam dengan masa lalu yang angker dan seorang novisiat di ambang sumpah terakhirnya dikirim oleh Vatikan untuk diselidiki. Bersama-sama mereka mengungkap rahasia rahasia yang tidak suci. Mempertaruhkan tidak hanya kehidupan mereka tetapi iman mereka dan jiwa mereka.'),
(5, 'Brave', 'brave.png', 'animasi', 'USA', '2012-06-21', 9, 50000, 'Berani diatur dalam mistis Dataran Tinggi Skotlandia, di mana Mérida adalah putri dari kerajaan diperintah oleh Raja Fergus dan Ratu Elinor. Putri nakal dan seorang pemanah ulung, Mérida satu hari menentang kebiasaan suci tanah dan secara tidak sengaja membawa gejolak ke kerajaan. Dalam upaya untuk mengatur hal-hal yang benar, Mérida mencari seorang eksentrik berusia Wanita Bijaksana dan diberikan keinginan naas. '),
(6, 'Dory', 'dory.png', 'animasi', 'USA', '2016-06-16', 5, 55000, '\"Finding Dory \" reuni Dory dengan teman-teman Nemo dan Marlin pada pencarian jawaban tentang masa lalunya. Apa yang bisa dia ingat? Siapa orangtuanya? Dan di mana dia belajar berbicara Whale?'),
(7, 'Despicable Me', 'despicableme.png', 'animasi', 'USA', '2010-07-08', 3, 50000, 'Di lingkungan pinggiran bahagia dikelilingi oleh pagar kayu putih dengan berbunga mawar, duduk rumah hitam dengan rumput mati. Tanpa diketahui para tetangga, tersembunyi jauh di bawah rumah ini adalah tempat persembunyian rahasia besar. Dikelilingi oleh tentara tak kenal lelah, pelayan kuning kecil, kita menemukan Gru, merencanakan pencurian terbesar dalam sejarah dunia.'),
(8, 'Ralp Break The Internet', 'RPTI.png', 'animasi', 'USA', '2018-11-21', 8, 70000, 'Taking place six years following the events of the first film, the story will center on Ralph\'s adventures in the Internet data space when a Wi-Fi router gets plugged into the arcade as he must find a replacement part to fix Sugar Rush.');

-- --------------------------------------------------------

--
-- Table structure for table `tabel_user`
--

CREATE TABLE `tabel_user` (
  `id_user` int(11) NOT NULL,
  `username` varchar(20) NOT NULL,
  `password` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

--
-- Dumping data for table `tabel_user`
--

INSERT INTO `tabel_user` (`id_user`, `username`, `password`) VALUES
(1, 'user1', '12345');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tabel_customer`
--
ALTER TABLE `tabel_customer`
  ADD PRIMARY KEY (`id_customer`);

--
-- Indexes for table `tabel_film`
--
ALTER TABLE `tabel_film`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_genre` (`genre`);

--
-- Indexes for table `tabel_user`
--
ALTER TABLE `tabel_user`
  ADD PRIMARY KEY (`id_user`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tabel_film`
--
ALTER TABLE `tabel_film`
  MODIFY `id` int(3) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
